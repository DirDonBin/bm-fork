﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BK_Fork.Parsers.Model;
using BK_Fork.Parsers.Parsers;
using BK_Fork.Parsers.Parsers.Baltbet;
using BK_Fork.Parsers.Parsers.BetCity;
using BK_Fork.Parsers.Parsers.Stavka1X;

namespace BK_Fork.Parsers
{
    public class ControlParsers
    {
        private ParserSettingsModel Settings { get; set; }
        private HttpClient Client;

        #region Parsers

        private Stavka1XParser stavka1XParser = new Stavka1XParser();
        private BaltbetParser baltbetParser = new BaltbetParser();
        private BetCityParser betCityParser = new BetCityParser();

        #endregion

        public ControlParsers(ParserSettingsModel settings = null)
        {
            if (settings == null)
            {
                settings = new ParserSettingsModel
                {
                    Actives = SitesMethod.GetAll(),
                    TimeDelay = 10,
                    MaxCountGame = 5
                };
            }

            Settings = settings;
            
        }

        public List<ParserModel> StartParser(ParserFilter filter = null)
        {
            var resultParsers = new List<ParserModel>();
            var result1XStavka = new List<ParserModel>();
            var resultBetcity = new List<ParserModel>();

            if (Settings.Actives[SitesName.Stavka1X.ToString()])
            {
                result1XStavka.AddRange(Task.Run(async () => await stavka1XParser.Parse(filter)).Result);
            }
            if (Settings.Actives[SitesName.BetCity.ToString()])
            {
                resultBetcity.AddRange(Task.Run(async () => await betCityParser.Parse(filter)).Result);
            }
            
            resultParsers.AddRange(result1XStavka);
            resultParsers.AddRange(resultBetcity);

            return resultParsers;
        }
    }
}
