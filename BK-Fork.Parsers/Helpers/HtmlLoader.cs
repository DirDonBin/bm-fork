﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;

namespace BK_Fork.Parsers.Helpers
{
    public class HtmlLoader
    {
        private readonly HttpClient client;
        private string url;

        public HtmlLoader(string url)
        {
            client = new HttpClient();
            this.url = url;
        }

        public async Task<string> GetDocument(IConfiguration config = null)
        {
            var response = await client.GetAsync(url);
            string source = null;

            if (response != null && response.StatusCode == HttpStatusCode.OK)
            {
                source = await response.Content.ReadAsStringAsync();
            }

            HtmlParser domParser;

            if (config != null)
            {
                var document = await BrowsingContext.New(config).OpenAsync(m => m.Content(source));
            }
            else
            {
                domParser = new HtmlParser();
                var document = await domParser.ParseAsync(source);
            }


            return source;
        }
    }
}
