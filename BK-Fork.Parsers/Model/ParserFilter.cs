﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Model
{
    public class ParserFilter
    {
        public bool Active { get; set; }
        public Dictionary<string, bool> Types { get; set; } = TypesGameMethod.GetAll();
    }
}
