﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Model
{
    public class ParserModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string TypeName { get; set; }
        public TypesGame TypeGame { get; set; }
        public string SiteName { get; set; }
        public ParserRatiosModel Ratios { get; set; }
        public bool Active { get; set; }
        public int GameCount { get; set; }
        public string Url { get; set; }
        public string Team1 { get; set; }
        public string Team2 { get; set; }

        public ParserSettingsModel ParserFilter { get; set; }
    }
}
