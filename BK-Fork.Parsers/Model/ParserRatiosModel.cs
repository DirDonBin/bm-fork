﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Model
{
    public class ParserRatiosModel
    {
        public Dictionary<double, double> TotalB { get; set; } = new Dictionary<double, double>();
        public Dictionary<double, double> TotalM { get; set; } = new Dictionary<double, double>();
        public Dictionary<double, double> PersonalTotalB1 { get; set; } = new Dictionary<double, double>();
        public Dictionary<double, double> PersonalTotalM1 { get; set; } = new Dictionary<double, double>();
        public Dictionary<double, double> PersonalTotalB2 { get; set; } = new Dictionary<double, double>();
        public Dictionary<double, double> PersonalTotalM2 { get; set; } = new Dictionary<double, double>();
        public Dictionary<string, double> Win { get; set; } = new Dictionary<string, double>();
        public Dictionary<string, double> Win2 { get; set; } = new Dictionary<string, double>();
    }
}
