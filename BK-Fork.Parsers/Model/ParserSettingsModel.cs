﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Model
{
    public class ParserSettingsModel
    {
        public Dictionary<string, bool> Actives { get; set; } = SitesMethod.GetAll();
        public int TimeDelay { get; set; }
        public int MaxCountGame { get; set; }
    }
}
