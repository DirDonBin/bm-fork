﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Model
{
    public enum SitesName
    {
        Stavka1X,
        Fonbet,
        Mostbet,
        Baltbet,
        LigaStavok,
        BetCity
    }

    public static class SitesMethod
    {
        public static Dictionary<string, bool> GetAll()
        {
            var result = new Dictionary<string, bool>();

            var sitesName = Enum.GetNames(typeof(SitesName));

            foreach (var siteName in sitesName)
            {
                result.Add(siteName, true); 
            }

            return result;
        }
    }
}
