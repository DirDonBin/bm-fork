﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Model
{
    public enum TypesGame
    {
        Football,
        Hockey,
        Basketball,
        ESports,
        Tennis,
        Other
    }

    public static class TypesGameMethod
    {
        public static Dictionary<string, bool> GetAll()
        {
            var result = new Dictionary<string, bool>();

            var sitesName = Enum.GetNames(typeof(TypesGame));

            foreach (var siteName in sitesName)
            {
                result.Add(siteName, true);
            }

            return result;
        }

        public static string GetNameType(TypesGame type)
        {
            switch (type)
            {
                case TypesGame.Basketball:
                    return "basketball";
                case TypesGame.Football:
                    return "soccer";
                case TypesGame.ESports:
                    return "cybersport";
                case TypesGame.Hockey:
                    return "ice-hockey";
                case TypesGame.Tennis:
                    return "tennis";
                default: return "";
            }
        }

        public static TypesGame GetTypeRus(string typeGame)
        {
            switch (typeGame.ToLower())
            {
                case "киберспорт":
                    return TypesGame.ESports;
                case "футбол":
                    return TypesGame.Football;
                case "хоккей":
                    return TypesGame.Hockey;
                case "теннис":
                    return TypesGame.Tennis;
                case "баскетбол":
                    return TypesGame.Basketball;
                default: return TypesGame.Other;
            }
        }

        public static TypesGame GetTypeEng(string typeGame)
        {
            switch (typeGame.ToLower())
            {
                case "cybersport":
                    return TypesGame.ESports;
                case "soccer":
                    return TypesGame.Football;
                case "ice-hockey":
                    return TypesGame.Hockey;
                case "tennis":
                    return TypesGame.Tennis;
                case "basketball":
                    return TypesGame.Basketball;
                default: return TypesGame.Other;
            }
        }
    }
}
