﻿using AngleSharp;
using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using BK_Fork.Parsers.Helpers;
using BK_Fork.Parsers.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Parsers.Baltbet
{
    public class BaltbetParser
    {
        public async Task<List<ParserModel>> Parse()
        {
            var resultsParser = new List<ParserModel>();

            var url = "https://www.baltbet.ru/";

            var config = Configuration.Default.WithDefaultLoader().WithJavaScript();

            var document = await new HtmlLoader(url).GetDocument(config);

            var parser = new HtmlParser();

            var site = await parser.ParseAsync(document);

            var liveGame = site.QuerySelectorAll(".live a");

            foreach (var game in liveGame)
            {
                var gameLink = (IHtmlLinkElement)game;
                gameLink.DoClick();
            }   

            return resultsParser;
        }
    }
}
