﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BK_Fork.Parsers.Model;
using BK_Fork.Parsers.Parsers.Stavka1X;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BK_Fork.Parsers.Parsers.BetCity
{
    public class BetCityParser
    {
        HttpClientHandler handler = new HttpClientHandler()
        {
            AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
        };

        public async Task<List<ParserModel>> Parse(ParserFilter filter)
        {
            var parserResultList = new List<ParserModel>();

            var sportsType = new List<int>();

            if (filter != null)
            {
                foreach (var type in filter.Types)
                {
                    if (type.Value && type.Key.ToLower() == TypesGame.Basketball.ToString().ToLower())
                    {
                        sportsType.Add(3);
                    }
                    if (type.Value && type.Key.ToLower() == TypesGame.ESports.ToString().ToLower())
                    {
                        sportsType.Add(73);
                    }
                    if (type.Value && type.Key.ToLower() == TypesGame.Football.ToString().ToLower())
                    {
                        sportsType.Add(1);
                    }
                    if (type.Value && type.Key.ToLower() == TypesGame.Hockey.ToString().ToLower())
                    {
                        sportsType.Add(7);
                    }
                    if (type.Value && type.Key.ToLower() == TypesGame.Tennis.ToString().ToLower())
                    {
                        sportsType.Add(2);
                    }
                }
            }
            else
            {
                sportsType.Add(1);
                sportsType.Add(2);
                sportsType.Add(3);
                sportsType.Add(7);
                sportsType.Add(73);
            }
            
            var url = "https://ad.betcity.ru/d/on_air/bets?rev=7";
            var client = new HttpClient(handler);

            var response = await client.GetStringAsync(url);

            dynamic data = JObject.Parse(response);
            
            var sports = data.reply.sports;

            var sportsList = new List<dynamic>();
            
            // составление списка игр
            foreach (var item in sports)
            {
                var value = (int)item.First.id_sp.Value;
                if (sportsType.Contains(value))
                {
                    sportsList.Add(item.First);
                }
            }

            var gameList = new List<dynamic>();

            // составление списка матчей
            foreach (var item in sportsList)
            {
                gameList.AddRange(item.chmps);
            }
            
            foreach (var game in gameList)
            {
               parserResultList.AddRange(await GetGames(game));
            }

            parserResultList.RemoveAll(x => x == null);
            parserResultList = parserResultList.Distinct().ToList();

            return parserResultList;
        }

        private async Task<List<ParserModel>> GetGames(dynamic game)
        {

            var type = new TypesGame();

            var typeNameRus = ((string)game.First.name_ch.ToString()).Split('.')[0];

            switch (typeNameRus)
            {
                case "Киберспорт":
                    type = TypesGame.ESports;
                    break;
                case "Футбол":
                    type = TypesGame.Football;
                    break;
                case "Хоккей":
                    type = TypesGame.Hockey;
                    break;
                case "Теннис":
                    type = TypesGame.Tennis;
                    break;
                case "Баскетбол":
                    type = TypesGame.Basketball;
                    break;
            }

            var result = new List<ParserModel>();

            

             foreach (var eventGame in game.First.evts)
            {
                result.Add(await GetInfoGame(eventGame, type, game.Name));
            }

            return result;
        }

        private async Task<ParserModel> GetInfoGame(dynamic game, TypesGame typeName, string typeId)
        {
            if (game.First.id_dep != null)
                return null;

            var gameInfo = new ParserModel
            {
                SiteName = SitesName.BetCity.ToString(),
                TypeName = TypesGameMethod.GetNameType(typeName)
            };

            gameInfo.TypeGame = TypesGameMethod.GetTypeEng(gameInfo.TypeName);

            gameInfo.Id = (int) game.First.id_ev;
            gameInfo.Url = $"https://betcity.ru/ru/live/{gameInfo.TypeName}/{typeId}/{gameInfo.Id}";
            gameInfo.Team1 = (string)game.First.name_ht?.ToString();
            gameInfo.Team2 = (string)game.First.name_at?.ToString();
            gameInfo.Type = typeId;

            gameInfo.Ratios = await GetRatios(game.First);

            return gameInfo;
        }

        private async Task<ParserRatiosModel> GetRatios(dynamic ratios)
        {
            var result = new ParserRatiosModel();

            await Task.Run(() =>
            {
                if (ratios.ext != null)
                {
                    foreach (var ratio in ratios.ext)
                    {
                        var gameName = (string) ratio.First.name.ToString();
                        if (gameName == "Индивидуальный тотал")
                        {
                            foreach (var item in ratio.First.data)
                            {
                                var stv = item.First.blocks;

                                if (stv.IT_T1 != null && (double)stv.IT_T1.Tot % 1 != 0)
                                {
                                    result.PersonalTotalB1.Add((double)stv.IT_T1.Tot, (double)stv.IT_T1.Tb.kf);
                                    result.PersonalTotalM1.Add((double)stv.IT_T1.Tot, (double)stv.IT_T1.Tm.kf);
                                }

                                if (stv.IT_T2 != null && (double)stv.IT_T2.Tot % 1 != 0)
                                {
                                    result.PersonalTotalB2.Add((double)stv.IT_T2.Tot, (double)stv.IT_T2.Tb.kf);
                                    result.PersonalTotalM2.Add((double)stv.IT_T2.Tot, (double)stv.IT_T2.Tm.kf);
                                }
                            }
                        }

                        if (gameName == "Тотал")
                        {
                            foreach (var item in ratio.First.data)
                            {
                                var stv = item.First.blocks.T;

                                if ((double)stv.Tot % 1 != 0)
                                {
                                    result.TotalB.Add((double)stv.Tot, (double)stv.Tb.kf);
                                    result.TotalM.Add((double)stv.Tot, (double)stv.Tm.kf);
                                }
                            }
                        }
                    }
                }
            });

            await Task.Run(() =>
            {
                if (ratios.main != null)
                {
                    foreach (var ratio in ratios.main)
                    {
                        var gameName = (string)ratio.First.name.ToString();
                        if (gameName == "Фактический исход")
                        {
                            foreach (var item in ratio.First.data)
                            {
                                var stv = item.First.blocks.Wm;

                                if (stv.P1 != null)
                                {
                                    result.Win.Add("п1", (double)stv.P1.kf);
                                }

                                if (stv.P2 != null)
                                {
                                    result.Win.Add("п2", (double)stv.P2.kf);
                                }
                            }
                        }

                        if (gameName == "Двойной исход")
                        {
                            foreach (var item in ratio.First.data)
                            {
                                var stv = item.First.blocks.WXm;

                                foreach (var st in stv)
                                {
                                    if (st.Name == "1X")
                                    {
                                        result.Win2.Add("1x", (double)st.First.kf);
                                    }

                                    if (st.Name == "X2")
                                    {
                                        result.Win2.Add("x2", (double)st.First.kf);
                                    }
                                }
                            }
                        }
                    }
                }
            });
            
            return result;
        }

    }
}
