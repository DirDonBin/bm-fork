﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Parsers.Stavka1X
{
    class JsonObjectCard
    {
        public class MIO
        {
            public string Loc { get; set; }
            public string TSt { get; set; }
            public string MaF { get; set; }
        }

        public class MI
        {
            public int K { get; set; }
            public string V { get; set; }
        }

        public class FS
        {
            public int S2 { get; set; }
            public int? S1 { get; set; }
        }

        public class S
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }

        public class SS
        {
            public string S1 { get; set; }
            public string S2 { get; set; }
        }

        public class Value2
        {
            public int ID { get; set; }
            public string S1 { get; set; }
            public string S2 { get; set; }
        }

        public class ST
        {
            public int Key { get; set; }
            public List<Value2> Value { get; set; }
        }

        public class SC
        {
            public int CP { get; set; }
            public string CPS { get; set; }
            public FS FS { get; set; }
            public int HC { get; set; }
            public List<object> PS { get; set; }
            public List<S> S { get; set; }
            public int TR { get; set; }
            public int TS { get; set; }
            public string I { get; set; }
            public int? GS { get; set; }
            public int? P { get; set; }
            public int? TD { get; set; }
            public SS SS { get; set; }
            public List<ST> ST { get; set; }
        }

        public class Value
        {
            public int CO { get; set; }
            public int COI { get; set; }
            public List<object> E { get; set; }
            public int EC { get; set; }
            public int HS { get; set; }
            public bool HSI { get; set; }
            public int I { get; set; }
            public string L { get; set; }
            public string LE { get; set; }
            public int LI { get; set; }
            public MIO MIO { get; set; }
            public List<MI> MIS { get; set; }
            public int N { get; set; }
            public string O1 { get; set; }
            public int O1C { get; set; }
            public string O1E { get; set; }
            public int O1I { get; set; }
            public List<int> O1IS { get; set; }
            public string O2 { get; set; }
            public int O2C { get; set; }
            public string O2E { get; set; }
            public int O2I { get; set; }
            public List<int> O2IS { get; set; }
            public int S { get; set; }
            public string SE { get; set; }
            public int SI { get; set; }
            public string SN { get; set; }
            public int SS { get; set; }
            public int SST { get; set; }
            public int T { get; set; }
            public string TN { get; set; }
            public int HMH { get; set; }
            public int R { get; set; }
            public SC SC { get; set; }
            public int ZP { get; set; }
            public int? VA { get; set; }
            public string VI { get; set; }
            public int? IV { get; set; }
            public string V { get; set; }
            public string VE { get; set; }
            public bool? F { get; set; }
            public bool? HEG { get; set; }
            public string DI { get; set; }
            public bool? HSRT { get; set; }
        }

        public class RootObject
        {
            public string Error { get; set; }
            public int ErrorCode { get; set; }
            public string Guid { get; set; }
            public int Id { get; set; }
            public bool Success { get; set; }
            public List<Value> Value { get; set; }
        }
    }
}
