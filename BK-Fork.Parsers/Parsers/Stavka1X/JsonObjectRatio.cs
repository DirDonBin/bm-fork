﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Parsers.Parsers.Stavka1X
{
    class JsonObjectRatio
    {
        public class Rat
        {
            public double C { get; set; }
            public double CE { get; set; }
            public double G { get; set; }
            public double P { get; set; }
            public double T { get; set; }
        }

        public class GE
        {
            public List<List<Rat>> E { get; set; }
            public int G { get; set; }
        }

        public class MIO
        {
            public string Loc { get; set; }
            public string TSt { get; set; }
        }

        public class MI
        {
            public int K { get; set; }
            public string V { get; set; }
        }

        public class FS
        {
            public int S1 { get; set; }
            public int S2 { get; set; }
        }

        public class Value2
        {
            public int S1 { get; set; }
            public int S2 { get; set; }
        }

        public class P
        {
            public int Key { get; set; }
            public Value2 Value { get; set; }
        }

        public class S
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }

        public class SC
        {
            public int CP { get; set; }
            public string CPS { get; set; }
            public FS FS { get; set; }
            public int HC { get; set; }
            public List<P> PS { get; set; }
            public List<S> S { get; set; }
            public int TS { get; set; }
        }

        public class GE2
        {
            public List<List<Rat>> E { get; set; }
            public int G { get; set; }
        }

        public class SG
        {
            public int EC { get; set; }
            public int EGC { get; set; }
            public List<GE2> GE { get; set; }
            public int I { get; set; }
            public int N { get; set; }
            public string TG { get; set; }
            public int TI { get; set; }
            public int R { get; set; }
            public int? T { get; set; }
        }

        public class Value
        {
            public int CO { get; set; }
            public int COI { get; set; }
            public int EC { get; set; }
            public int EGC { get; set; }
            public List<GE> GE { get; set; }
            public bool HSI { get; set; }
            public int I { get; set; }
            public string L { get; set; }
            public string LE { get; set; }
            public int LI { get; set; }
            public MIO MIO { get; set; }
            public List<MI> MIS { get; set; }
            public int N { get; set; }
            public string O1 { get; set; }
            public int O1C { get; set; }
            public string O1E { get; set; }
            public int O1I { get; set; }
            public List<int> O1IS { get; set; }
            public string O2 { get; set; }
            public int O2C { get; set; }
            public string O2E { get; set; }
            public int O2I { get; set; }
            public List<int> O2IS { get; set; }
            public int S { get; set; }
            public string SE { get; set; }
            public int SI { get; set; }
            public string SN { get; set; }
            public int SS { get; set; }
            public int SST { get; set; }
            public int T { get; set; }
            public string TN { get; set; }
            public int CBR { get; set; }
            public bool COP { get; set; }
            public int CS { get; set; }
            public int HMH { get; set; }
            public int R { get; set; }
            public SC SC { get; set; }
            public List<SG> SG { get; set; }
            public int ZP { get; set; }
        }

        public class RootObject
        {
            public string Error { get; set; }
            public int ErrorCode { get; set; }
            public string Guid { get; set; }
            public int Id { get; set; }
            public bool Success { get; set; }
            public Value Value { get; set; }
        }
    }
}
