﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BK_Fork.Parsers.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BK_Fork.Parsers.Parsers.Stavka1X
{
    public class Stavka1XParser
    {
        HttpClientHandler handler = new HttpClientHandler()
        {
            AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
        };

        public async Task<List<ParserModel>> Parse(ParserFilter filter)
        {
            var parserResultList = new List<ParserModel>();

            var url = "https://1xstavka.ru/LiveFeed/Get1x2_VZip?count=150&mode=4&country=1";
            var client = new HttpClient(handler);

            var response = await client.GetStringAsync(url);
          
            var values = JsonConvert.DeserializeObject<JsonObjectCard.RootObject>(response);

            await Task.Run(async () => {
                foreach (var item in values.Value.Where(x => x.HSI))
                {
                    var parserResult = new ParserModel
                    {
                        Id = item.I,
                        Type = item.SE,
                        TypeName = item.SN,

                        TypeGame = TypesGameMethod.GetTypeRus(item.SN) != TypesGame.Other
                            ? TypesGameMethod.GetTypeRus(item.SN)
                            : TypesGameMethod.GetTypeEng(item.SE),

                        SiteName = SitesName.Stavka1X.ToString(),
                        Team1 = item.O1,
                        Team2 = item.O2,
                    };

                    var typesGame = TypesGameMethod.GetAll();

                    if (typesGame.ContainsKey(parserResult.Type) && typesGame[parserResult.Type])
                    {
                        var baseUrl = "https://1xstavka.ru/live/";

                        var ligaName = item.LE;
                        var ligaID = item.LI;
                        var team1 = item.O1E;
                        var team2 = item.O2E;

                        if (team1 == null || team2 == null)
                            continue;

                        ligaName = ligaName.Replace(".", string.Empty);
                        ligaName = ligaName.Replace(' ', '-');
                        ligaName = ligaName.Replace("(", "");
                        ligaName = ligaName.Replace(")", "");
                        baseUrl += parserResult.Type + '/' + ligaID + '-' + ligaName + '/';

                        team1 = team1.Replace(' ', '-');
                        team1 = team1.Replace("(", "");
                        team1 = team1.Replace(")", "");
                        team1 = team1.Replace("/", "");
                        team1 = Regex.Replace(team1, "[^a-zA-Z-]", "");
                        
                        team2 = team2.Replace(' ', '-');
                        team2 = team2.Replace("(", "");
                        team2 = team2.Replace(")", "");
                        team2 = team2.Replace("/", "");
                        team2 = Regex.Replace(team2, "[^a-zA-Z-]", "");

                        baseUrl += parserResult.Id.ToString() + '-' + team1 + '-' + team2 + '/';

                        parserResult.Url = baseUrl;

                        parserResult.Ratios = await GetRatios(parserResult);

                        parserResultList.Add(parserResult);
                    }
                }
            });

            return parserResultList;
        }

        private async Task<ParserRatiosModel> GetRatios(ParserModel model)
        {
            var url = $"https://1xstavka.ru/LiveFeed/GetGameZip?id={model.Id}&lng=ru&cfview=0&isSubGames=true&GroupEvents=true&allEventsGroupSubGames=true&countevents=250";

            var client = new HttpClient(handler);
            var response = await client.GetStringAsync(url);

            var values = JsonConvert.DeserializeObject<JsonObjectRatio.RootObject>(response);

            var ratios = new ParserRatiosModel();

            var bets = values?.Value?.GE?.SelectMany(x => x.E).SelectMany(x => x).ToList();

            foreach (var betsItem in bets)
            {
                await Task.Run(() =>
                {

                    switch (betsItem.T)
                    {
                        case 1:
                            ratios.Win.Add("п1", betsItem.C);
                            break;
                        //case 2:
                        //    ratios.Win.Add("н", betsItem.C);
                        //    break;
                        case 3:
                            ratios.Win.Add("п2", betsItem.C);
                            break;
                        case 4:
                            ratios.Win2.Add("1x", betsItem.C);
                            break;
                        case 6:
                            ratios.Win2.Add("x2", betsItem.C);
                            break;
                        case 9:
                            if (betsItem.P % 1 != 0)
                                ratios.TotalB.Add(betsItem.P, betsItem.C);
                            break;
                        case 10:
                            if (betsItem.P % 1 != 0)
                                ratios.TotalM.Add(betsItem.P, betsItem.C);
                            break;
                        case 11:
                            if (betsItem.P % 1 != 0)
                                ratios.PersonalTotalB1.Add(betsItem.P, betsItem.C);
                            break;
                        case 12:
                            if (betsItem.P % 1 != 0)
                                ratios.PersonalTotalM1.Add(betsItem.P, betsItem.C);
                            break;
                        case 13:
                            if (betsItem.P % 1 != 0)
                                ratios.PersonalTotalB2.Add(betsItem.P, betsItem.C);
                            break;
                        case 14:
                            if (betsItem.P % 1 != 0)
                                ratios.PersonalTotalM2.Add(betsItem.P, betsItem.C);
                            break;
                        case 3656:
                            ratios.Win2.Add("1x", betsItem.C);
                            break;
                        //case 3657:
                        //    ratios.Win2.Add("12", betsItem.C);
                        //    break;
                        case 3658:
                            ratios.Win2.Add("x2", betsItem.C);
                            break;
                    }
                });
            }

            return ratios;
        }
    }
}
