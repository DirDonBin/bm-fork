﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Media;
using BK_Fork.Model;
using BK_Fork.Parsers;
using BK_Fork.Parsers.Model;

namespace BK_Fork.Controller
{
    public static class ForkResultController
    {
        private static int sum;

        public static List<Fork> StartCalcFork(int summ)
        {
            sum = summ;

            var resultParsers = new ControlParsers().StartParser();

            var forks = GetListForks(resultParsers);

            return forks;
        }

        private static List<Fork> GetListForks(List<ParserModel> parserResultList)
        { 
            var listForks = new List<Fork>();
            
            var games = parserResultList.GroupBy(x => x.Team1 + x.Team2 + x.TypeGame).ToList();
            
            var gamesForFork = games.Where(x => x.Count() > 1).ToList();

            foreach (var game in gamesForFork)
            {
                listForks.AddRange(Task.Run(async () => await GetForks(game.ToList())).Result);
            }

            listForks = listForks.Where(x => x != null).ToList();

            return listForks;
        }

        private async static Task<List<Fork>> GetForks(List<ParserModel> games)
        {
            var forks = new List<Fork>();
            
            if (games.Where(x => x.Ratios.Win.Count > 0 && x.Ratios.Win2.Count > 0).ToList().Count() > 1)
            {
                await Task.Run(() => forks.AddRange(GetForkForWin(games)));
            }

            if (games.Where(x => x.Ratios.TotalB.Count > 0 || x.Ratios.TotalM.Count > 0).ToList().Count() > 1)
            {
                await Task.Run(() => forks.AddRange(GetForkForTotal(games)));
            }

            return forks;
        }

        /// <summary>
        /// Получение вилки на Фактический - Двойной исход
        /// </summary>
        /// <param name="games"></param>
        /// <returns></returns>
        private static List<Fork> GetForkForWin(List<ParserModel> games)
        {
            var ratios = games.ToDictionary(x => x.SiteName, t => t.Ratios.Win.ContainsKey("п1") ? t.Ratios.Win["п1"] : -1);
            var ratio = ratios.First(x => x.Value == ratios.Max(t => t.Value));
            var game1 = games.First(x => x.SiteName == ratio.Key);
            
            ratios = games.Where(x => x != game1).ToDictionary(x => x.SiteName, t => t.Ratios.Win2.ContainsKey("x2") ? t.Ratios.Win2["x2"] : -1);
            ratio = ratios.FirstOrDefault(x => x.Value == ratios.Max(t => t.Value));
            var game21 = games.First(x => x.SiteName == ratio.Key);

            ratios = games.Where(x => x != game1).ToDictionary(x => x.SiteName, t => t.Ratios.Win.ContainsKey("п2") ? t.Ratios.Win["п2"] : -1);
            ratio = ratios.FirstOrDefault(x => x.Value == ratios.Max(t => t.Value));
            var game2 = games.First(x => x.SiteName == ratio.Key);
            
            ratios = games.Where(x => x != game2).ToDictionary(x => x.SiteName, t => t.Ratios.Win2.ContainsKey("1x") ? t.Ratios.Win2["1x"] : -1);
            ratio = ratios.FirstOrDefault(x => x.Value == ratios.Max(t => t.Value));
            var game22 = games.First(x => x.SiteName == ratio.Key);

            double rate1;
            double rate2;

            if (game1.Ratios.Win.ContainsKey("п1") && game21.Ratios.Win2.ContainsKey("x2"))
            {
                rate1 = 1 / game1.Ratios.Win["п1"] + 1 / game21.Ratios.Win2["x2"];
            }
            else
            {
                rate1 = 404;
            }

            if (game2.Ratios.Win.ContainsKey("п2") && game22.Ratios.Win2.ContainsKey("1x"))
            {
                rate2 = 1 / game2.Ratios.Win["п2"] + 1 / game22.Ratios.Win2["1x"];
            }
            else
            {
                rate2 = 404;
            }
            
            var forks = new List<Fork>();

            if (rate1 <= 1)
            {
                var fork = new Fork
                {
                    GameName = game1.Team1 + " - " + game1.Team2,
                    Ratio1 = game1.Ratios.Win["п1"],
                    Ratio2 = game21.Ratios.Win2["x2"],
                    SiteBM1 = game1.SiteName,
                    SiteBM2 = game21.SiteName,
                    BetName = "Фактический - Двойной исход",
                    RatioName1 = "п1",
                    RatioName2 = "x2",
                    Url1 = game1.Url,
                    Url2 = game21.Url,
                    Rate = rate1
                };

                fork = GetBet(fork, 10, rate1);

                //fork.Bet1 = (int)(sum / (rate1 * fork.Ratio1));
                //fork.Bet2 = (int)(sum / (rate1 * fork.Ratio2));

                fork.Summa = fork.Bet1 + fork.Bet2;
                fork.ResultWin1 = Math.Round(fork.Bet1 * fork.Ratio1 - fork.Summa, 2);
                fork.ResultWin2 = Math.Round(fork.Bet2 * fork.Ratio2 - fork.Summa, 2);

                var percent1 = fork.ResultWin1 / fork.Summa * 100;
                var percent2 = fork.ResultWin2 / fork.Summa * 100;
                fork.PercentWin = percent1 > percent2 ? (int)Math.Round(percent1) : (int)Math.Round(percent2);

                if (fork.PercentWin <= 15)
                    forks.Add(fork);
            }

            if (rate2 <= 1)
            {
                var fork = new Fork
                {
                    GameName = game2.Team1 + " - " + game2.Team2,
                    Ratio1 = game2.Ratios.Win["п2"],
                    Ratio2 = game22.Ratios.Win2["1x"],
                    SiteBM1 = game2.SiteName,
                    SiteBM2 = game22.SiteName,
                    BetName = "Фактический - Двойной исход",
                    RatioName1 = "п2",
                    RatioName2 = "1x",
                    Url1 = game2.Url,
                    Url2 = game22.Url,
                    Rate = rate2
                };

                fork = GetBet(fork, 10, rate2);

                //fork.Bet1 = (int)(sum / (rate2 * fork.Ratio1));
                //fork.Bet2 = (int)(sum / (rate2 * fork.Ratio2));

                fork.Summa = fork.Bet1 + fork.Bet2;
                fork.ResultWin1 = Math.Round(fork.Bet1 * fork.Ratio1 - fork.Summa, 2);
                fork.ResultWin2 = Math.Round(fork.Bet2 * fork.Ratio2 - fork.Summa, 2);

                var percent1 = fork.ResultWin1 / fork.Summa * 100;
                var percent2 = fork.ResultWin2 / fork.Summa * 100;
                fork.PercentWin = percent1 > percent2 ? (int)Math.Round(percent1) : (int)Math.Round(percent2);

                if (fork.PercentWin <= 15)
                    forks.Add(fork);
            }
            
            return forks;
        }
        
        /// <summary>
        /// Получение вилок по тоталам
        /// </summary>
        /// <param name="games"></param>
        /// <returns></returns>
        private static List<Fork> GetForkForTotal(List<ParserModel> games)
        {
            var ratiosTotalB = new List<RatiosTotal>();
            var ratiosTotalM = new List<RatiosTotal>();

            foreach (var game in games)
            {
                foreach (var ratio in game.Ratios.TotalB)
                {
                    ratiosTotalB.Add(new RatiosTotal{SiteName = game.SiteName, Bet = ratio.Key, Ratio = ratio.Value});
                }

                foreach (var ratio in game.Ratios.TotalM)
                {
                    ratiosTotalM.Add(new RatiosTotal { SiteName = game.SiteName, Bet = ratio.Key, Ratio = ratio.Value });
                }
            }

            ratiosTotalB = ratiosTotalB.GroupBy(x => x.Bet).Where(x => x.Count() > 1).SelectMany(x => x).ToList();
            var ratiosB = ratiosTotalB.GroupBy(x => x.Bet).ToDictionary(x => x.Key, t => t.ToList()).Values;

            var maxBetsB = ratiosB.Select(x => x.Where(t => t.Ratio == x.Max(q => q.Ratio))).SelectMany(x => x).ToList();
            

            ratiosTotalM = ratiosTotalM.GroupBy(x => x.Bet).Where(x => x.Count() > 1).SelectMany(x => x).ToList();
            var ratiosM = ratiosTotalM.GroupBy(x => x.Bet).ToDictionary(x => x.Key, t => t.ToList()).Values;

            var maxBetsM = ratiosM.Select(x => x.Where(t => t != maxBetsB.Where(q => q.Bet == t.Bet && q.SiteName == t.SiteName) 
                                                            && t.Ratio == x.Max(q => q.Ratio))).SelectMany(x => x).ToList();

            var ratiosForForks = maxBetsB.Join(maxBetsM,
                                                x => x.Bet,
                                                p => p.Bet,
                                                (x, p) => new
                                                {
                                                    SiteNameB = x.SiteName,
                                                    SiteNameM = p.SiteName,
                                                    Bet = x.Bet,
                                                    RatioB = x.Ratio,
                                                    RatioM = p.Ratio
                                                }).ToList();

            var forks = new List<Fork>();

            foreach (var ratio in ratiosForForks)
            {
                var rate = 1 / ratio.RatioB + 1 / ratio.RatioM;

                if(rate >= 1)
                    continue;

                var game1 = games.First(x => x.SiteName == ratio.SiteNameB);
                var game2 = games.First(x => x.SiteName == ratio.SiteNameM);

                var fork = new Fork
                {
                    SiteBM1 = ratio.SiteNameB,
                    SiteBM2 = ratio.SiteNameM,
                    BetName = $"Тотал {ratio.Bet}",
                    RatioName1 = "Бол.",
                    RatioName2 = "Мен.",
                    Ratio1 = ratio.RatioB,
                    Ratio2 = ratio.RatioM,
                    Rate = rate,
                    GameName = $"{game1.Team1} - {game1.Team2}",
                    Url1 = game1.Url,
                    Url2 = game2.Url
                };

                fork = GetBet(fork, 10, rate);

                //fork.Bet1 = (int)(sum / (rate * fork.Ratio1));
                //fork.Bet2 = (int)(sum / (rate * fork.Ratio2));

                fork.Summa = fork.Bet1 + fork.Bet2;
                fork.ResultWin1 = Math.Round(fork.Bet1 * fork.Ratio1 - fork.Summa, 2);
                fork.ResultWin2 = Math.Round(fork.Bet2 * fork.Ratio2 - fork.Summa, 2);

                var percent1 = fork.ResultWin1 / fork.Summa * 100;
                var percent2 = fork.ResultWin2 / fork.Summa * 100;
                fork.PercentWin = percent1 > percent2 ? (int)Math.Round(percent1) : (int)Math.Round(percent2);

                if (fork.PercentWin > 15)
                    continue;

                forks.Add(fork);
            }
            
            return forks;
        }

        private static Fork GetBet(Fork fork, int multiplier, double rate)
        {
            var bet1Tmp = Math.Round(sum / (rate * fork.Ratio1), 2);
            var bet2Tmp = Math.Round(sum / (rate * fork.Ratio2), 2);

            if (multiplier != 0 && multiplier != 1)
            {
            var bet1Rest = bet1Tmp - (int)bet1Tmp;
            var bet2Sum = bet1Rest;
            bet1Tmp -= bet1Rest;
            bet1Rest = bet1Tmp % multiplier;

            if (bet1Rest != 0)
            {
                if (bet1Rest >= multiplier / 2)
                {
                    bet1Tmp += multiplier - bet1Rest;
                    bet2Sum -= multiplier - bet1Rest;
                    
                }
                else
                {
                    bet1Tmp -= bet1Rest;
                    bet2Sum += bet1Rest;
                }
                bet2Tmp += bet2Sum;
            }

            fork.Bet1 = (int)Math.Round(bet1Tmp);
            fork.Bet2 = (int)Math.Round(bet2Tmp);
            }
            return fork;
        }

    }
}
