﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Model
{
    public class Fork
    {
        public string GameName { get; set; }
        public string BetName { get; set; }
        public string RatioName1 { get; set; }
        public string RatioName2 { get; set; }
        public string SiteBM1 { get; set; }
        public string SiteBM2 { get; set; }
        public double Ratio1 { get; set; }
        public double Ratio2 { get; set; }
        public double Rate { get; set; }
        public string Url1 { get; set; }
        public string Url2 { get; set; }
        public int PercentWin { get; set; }
        public int Summa { get; set; }
        public double ResultWin1 { get; set; }
        public double ResultWin2 { get; set; }
        public int BetPrice { get; set; }
        public int Bet1 { get; set; }
        public int Bet2 { get; set; }
    }
}
