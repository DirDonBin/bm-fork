﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BK_Fork.Model
{
    public class RatiosTotal
    {
        public string SiteName { get; set; }
        public double Bet { get; set; }
        public double Ratio { get; set; }
    }
}
