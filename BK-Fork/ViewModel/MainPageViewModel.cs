﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using BK_Fork.Controller;
using BK_Fork.Helpers;
using BK_Fork.Model;

namespace BK_Fork.ViewModel
{
    public class MainPageViewModel: ObservableObject, INotifyPropertyChanged
    {
        public MainPageViewModel()
        {
            Task.Run(() => 
            {
                while (true)
                {
                    Forks = ForkResultController.StartCalcFork(Sum);
                }
            });
        }

        #region Property

        private List<Fork> _forks;
        private List<Fork> Forks
        {
            get => _forks;
            set
            {
                _forks = value;
                OnPropertyChanged();
            }
        }

        private List<Fork> _dataItems;

        public List<Fork> DataItems
        {
            get => _dataItems;
            set
            {
                _dataItems = value;
                OnPropertyChanged();
            }
        }


        public Fork ForkGame { get; set; }
        
        public int Sum { get; set; } = 1000;
        
        #endregion

        #region Command

        public ICommand GoToSiteCommand { get; } = new DelegateCommand(x => GoToSite((string)x));

        public ICommand RefreshCommand
        {
            get
            {
                return new DelegateCommand(x => Refresh());
            }
        }

        #endregion

        #region Command implementation

        private static void GoToSite(string url)
        {
            Process.Start(url);
        }

        private void Refresh()
        {
            //DataItems = Task.Run(async () => await ForkResultController.StartCalcFork(Sum)).Result; ;
            DataItems = Forks;
        }

        #endregion

        #region Notify 

        public new event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
