﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BK_Fork.Helpers;

namespace BK_Fork.ViewModel
{
    public class MainWindowViewModel
    {
        private MainWindow window = Application.Current.Windows.OfType<MainWindow>().FirstOrDefault();

        public MainWindowViewModel()
        {
            OpenMainPageCommand = new DelegateCommand(o => OpenMainPage());
            OpenSettingsPageCommand = new DelegateCommand(o => OpenSettingsPage());
            OpenMainPage();
        }

        #region Property



        #endregion

        #region Command

        public DelegateCommand OpenMainPageCommand { get; set; }
        public DelegateCommand OpenSettingsPageCommand { get; set; }

        #endregion

        #region Command implementation

        private void OpenMainPage()
        {
            window.MainGrid.Children.Clear();
            window.MainGrid.Children.Add(new View.MainPage());
        }

        private void OpenSettingsPage()
        {
            window.MainGrid.Children.Clear();
            window.MainGrid.Children.Add(new View.Settings());
        }

        #endregion

    }
}
